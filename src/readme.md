# Repositorio base para proyectos de Angular

En este repositorio se ha generado la estructura base para los proyectos de Angular utilizando el template de Angular pro. 
Se han separado los siguientes componentes.

  - Navbar  
  - Sidebar
  - Tabla simple
  - Tabla extendida con botones
  - Componente para muestra de errores de validacion.
  - DataTable con el plugin ngx-datatable cuya documentacion se encuentra en el siguiente enlace http://swimlane.github.io/ngx-datatable/#inline-edit
   
 Tambien se ha generado la clase para crear notificaciones con alertify, y mostrar el loader llamando a una sola funcion con un parametro boolean para mostrar y ocultar.

En cuanto a la estructura de carpetas, cada modulo o aplicacion debera generar una carpeta nueva con el nombre del modulo, dentro del cual se creara mediante el generador de angular un modulo nuevo, en dicho modulo se deben importar los componentes compartidos mencionados anteriormente, excepto el navbar y el sidebar. Ademas se debe importar el modulo creado dentro del modulo principal de la aplicacion. 

## Obtener el layout basico

Para lograr el layout basico se requiere que en el componente principal de cada modulo (main.component.html) se debe incluir el siguiente marcado html.
``` html
<div class="wrapper">
  <div class="sidebar" data-active-color="white" data-background-color="red" data-image="../assets/img/sidebar-1.jpg">
    <div class="sidebar-background" style="background-image: url(assets/img/sidebar-1.jpg)"></div>
    <app-sidebar ></app-sidebar>
  </div>
  <div class="main-panel">
    <app-navbar></app-navbar>
    <router-outlet></router-outlet>
  </div>
  <div *ngIf="" class="close-layer visible"></div>
</div>
```

Cada aplicacion debe contar con su propio archivo de ruteo, el cual debe tener un contenido similar al siguiente.
``` typescript
const cajasRoutes: Routes = [
  {
    path: 'cajas',
    component: MainComponent,
    children: [
      {path: 'dashboard', component: DashboardComponent},
      {path: '', redirectTo: '/cajas/dashboard', pathMatch: 'full'},
    ]
  }
];

export const CajasRoutes = RouterModule.forChild(cajasRoutes);
```
Dicho archivo se debe importar en el archivo donde se declara el modulo.

Dentro de esta estructura se incluye un prototipo basico del dashboard donde se muestra la forma de usar los componentes de tabla. Para obtener mas informacion sobre DataTable se ha proporcionado el enlace a su documentacion, ya que esta no forma parte de lo que se incluye en el template.

