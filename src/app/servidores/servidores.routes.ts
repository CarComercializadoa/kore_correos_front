import {RouterModule, Routes} from '@angular/router';
import {MainComponent} from './main.component';
import {ServidoresComponent} from './servidores/servidores.component';
import {ServiciosComponent} from './servicios/servicios.component';
import {ConfiguracionCuentasComponent} from './configuracion-cuentas/configuracion-cuentas.component';
import { AuthGuardService } from '../auth/auth-guard.service';
import {IndexComponent} from './index/index.component';


const servidoresRoutes: Routes = [
  {
    path: 'servicioCorreos',
    component: MainComponent,
    children: [
      {path: 'servidores', component: ServidoresComponent},
      {path: 'servicios', component: ServiciosComponent},
      {path: 'cuentas/:id', component: ConfiguracionCuentasComponent},
      {path: '', component: IndexComponent},
      {path: '**', redirectTo: '/servicioCorreos', pathMatch: 'full'},
    ],
    canActivate: [AuthGuardService]
  }
];

export const ServidoresRoutes = RouterModule.forChild(servidoresRoutes);
