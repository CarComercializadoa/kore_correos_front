import {Component, OnInit} from '@angular/core';
import {ServicioModel} from '../Models/ServicioModel';
import {ServicioCorreosService} from '../services/servicio-correos.service';
import {Notification} from '../../functions/notification';
import {ServidorService} from '../services/servidor.service';

import {MiServidorModel} from '../Models/MiServidorModel';
import {showLoader} from '../../functions/Utilities';
import swal from 'sweetalert2';

@Component({
  selector: 'app-servicios',
  templateUrl: './servicios.component.html',
  styleUrls: []
})
export class ServiciosComponent implements OnInit {

  private Notificacion = new Notification();
  public servicioToUpd = new ServicioModel();
  public servicios: ServicioModel[] = [];
  // Vars Update
  public servidoresDisponibles: MiServidorModel [];
  public misServidoresUpdate: MiServidorModel[] = [];
  public isUpdate = false;


  constructor(private servicioCorreoService: ServicioCorreosService,
              private servidoresService: ServidorService) {

  }

  ngOnInit() {
    this.getServicios();
  }

  getServicios(): void {
    showLoader(true);
    this.servicioCorreoService.getServicios()
      .then(response => {
        return response.json();
      }).then(data => {
      showLoader(false);
      if (data.Codigo === 200) {
        if (!data.Data) {
          this.Notificacion.showNotification('Todavia no hay Servicios dados de alta', 'info');
        }
        this.servicios = data.Data;
      } else {
        this.Notificacion.showNotification(data.Mensaje, 'danger');
      }
    });
  }

  editar(servicio: ServicioModel) {
    if (this.isUpdate) {
      this.isUpdate = false;
    } else {
      this.servicioToUpd.cloneObject(servicio.ID, servicio.Token, servicio.Nombre,
        servicio.Servidores, servicio.Estatus, servicio.FechaHora);
      this.isUpdate = true;
    }
  }


  cancelarActualizacion(): void {
    this.isUpdate = false;
  }


  // Para eliminar
  eliminar(servicio: ServicioModel): void {
    var numCtas = 0;
    servicio.Servidores.filter(serv => {
      numCtas += serv.Cuentas.length;
    });

    swal({
      title: '¿Eliminar Servicio?',
      text: 'No podras recuperar el registro! \n ' + numCtas + ' Cuentas asociadas',
      type: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      confirmButtonText: 'Si, Eliminar!',
      buttonsStyling: false
    }).then(function () {
      // Eliminar de db
      this.deleter(servicio);
    }.bind(this)).catch(swal.noop);
  }

  deleter(servicio: ServicioModel): void {
    console.log(servicio);
    showLoader(true);
    this.servicioCorreoService.delete(servicio)
      .then(response => {
        return response.json();
      })
      .then(data => {
        showLoader(false);
        if (data.Codigo === 200) {

          // Filter
          const serviciosfil = this.servicios.filter(srv => {
            return srv.ID !== servicio.ID;
          });
          this.servicios = serviciosfil;

          swal({
            title: 'Eliminado!',
            text: 'Servicio eliminado.',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
            buttonsStyling: false
          });

        } else {
          console.log(data);
          this.getServicios();
          this.Notificacion.showNotification(data.Mensaje, 'danger');
        }
      });
  }
}
