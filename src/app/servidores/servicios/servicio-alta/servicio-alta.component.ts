import { Component, OnInit } from '@angular/core';
import {Notification} from '../../../functions/notification';
import {ServicioModel} from '../../Models/ServicioModel';
import {ServidorService} from '../../services/servidor.service';
import {ServicioCorreosService} from '../../services/servicio-correos.service';
import {MiServidorModel} from '../../Models/MiServidorModel';
import {ServidorModel} from '../../Models/ServidorModel';
import {ServiciosComponent} from '../servicios.component';
import {showLoader} from '../../../functions/Utilities';

@Component({
  selector: 'app-servicio-alta',
  templateUrl: './servicio-alta.component.html',
  styleUrls: []
})
export class ServicioAltaComponent implements OnInit {

  private Notificacion = new Notification();
  // Vars Alta
  public servicio = new ServicioModel();
  public servidores: MiServidorModel[] = [];

  constructor(private servicioCorreoService: ServicioCorreosService,
              private servidoresService: ServidorService,
              private componentePrincipal: ServiciosComponent) { }

  ngOnInit() {
    this.getServidores();
  }


  getServidores(): void {
    showLoader(true);
    this.servidoresService.getServidores()
      .then( response => {
        return response.json();
      }).then( data => {
      showLoader(false);
      if ( data.Codigo === 200 ) {
        this.servidores = this.casToMiServidorModel(data.Data);
      } else {
        this.Notificacion.showNotification(data.Mensaje, 'danger');
      }
    });
  }


  altaServicio(): void {
    showLoader(true);
    this.servicioCorreoService.add(this.servicio)
      .then( response => {
        return response.json();
      })
      .then( data => {
        showLoader(false);
        if ( data.Codigo === 200 ) {
          this.componentePrincipal.getServicios();
          this.servicio = new ServicioModel();
          this.Notificacion.showNotification(data.Mensaje, 'success');
        } else {
          this.Notificacion.showNotification(data.Mensaje, 'danger');
        }
      });
  }


  casToMiServidorModel(data: any[]): MiServidorModel[] {
    const arr: MiServidorModel[] = [];
    data.forEach( function (val, index) {
      const temp = new MiServidorModel();
      temp.ID = val.ID;
      temp.Servidor = val.Servidor;
      temp.Puerto = val.Puerto;
      temp.Orden = index;
      temp.MaxDest = val.MaxDest;
      temp.MaxCorreos = val.MaxCorreos;
      arr.push(temp);
    });
    return arr;
  }


}
