import {Component, Input, OnInit} from '@angular/core';
import {Notification} from '../../../functions/notification';
import {ServicioModel} from '../../Models/ServicioModel';
import {ServiciosComponent} from '../servicios.component';
import {ServidorService} from '../../services/servidor.service';
import {ServicioCorreosService} from '../../services/servicio-correos.service';
import {MiServidorModel} from '../../Models/MiServidorModel';
import {showLoader} from '../../../functions/Utilities';

@Component({
  selector: 'app-servicio-edita',
  templateUrl: './servicio-edita.component.html',
  styleUrls: []
})
export class ServicioEditaComponent implements OnInit {

  private Notificacion = new Notification();
  public servidores: MiServidorModel[] = [];

  @Input('servicio') servicio: ServicioModel;

  private servidoresSave: MiServidorModel[] = [];

  constructor(private servicioCorreoService: ServicioCorreosService,
              private servidoresService: ServidorService,
              private componentePrincipal: ServiciosComponent) { }

  ngOnInit() {
    this.getServidores();
  }


  getServidores(): void {
    showLoader(true);
    this.servidoresService.getServidores()
      .then( response => {
        return response.json();
      }).then( data => {
      showLoader(false);
      if ( data.Codigo === 200 ) {
        this.servidores = this.casToMiServidorModel(data.Data);
        this.servidores = this.filterServidoresDisponibles();
        this.servicio.Servidores = [];
      } else {
        this.Notificacion.showNotification(data.Mensaje, 'danger');
      }
    });
  }
  actualizaServicio(): void {
    showLoader(true);
    this.mergeElements();
    this.servicioCorreoService.update(this.servicio)
      .then(response => {
        return response.json();
      }).then( data => {
      showLoader(false);
      if ( data.Codigo === 200 ) {
        this.Notificacion.showNotification(data.Mensaje, 'success');
        this.componentePrincipal.getServicios();
        this.componentePrincipal.cancelarActualizacion();
      } else {
        this.Notificacion.showNotification(data.Mensaje, 'danger');
      }
    });
  }


  filterServidoresDisponibles(): MiServidorModel[] {
    const filtrados: MiServidorModel[] = [];
    this.servidores.filter( (servidorExistene) => {
      let exist = false;
        this.servicio.Servidores.filter( (miservidor) => {
          if ( miservidor.ID === servidorExistene.ID ) {
            const aux = new MiServidorModel();
            aux.cloneObject(miservidor.ID, miservidor.Orden, miservidor.Puerto, miservidor.Servidor,
              miservidor.MaxDest, miservidor.MaxCorreos, miservidor.Cuentas);
            this.servidoresSave.push(aux);
            exist = true;
          }
        });
      if ( !exist ) {
        filtrados.push(servidorExistene);
      }
    });
    return filtrados;
  }

  casToMiServidorModel(data: any[]): MiServidorModel[] {
    const arr: MiServidorModel[] = [];
    data.forEach( function (val, index) {
      const temp = new MiServidorModel();
      temp.ID = val.ID;
      temp.Servidor = val.Servidor;
      temp.Puerto = val.Puerto;
      temp.Orden = index;
      temp.MaxDest = val.MaxDest;
      temp.MaxCorreos = val.MaxCorreos;
      arr.push(temp);
    });
    return arr;
  }

  mergeElements() {
    console.log('Guardados');
    console.log(this.servidoresSave);
    console.log('seleccionados');
    console.log(this.servicio.Servidores);
    if ( this.servicio.Servidores ) {
      this.servidoresSave.filter( val => {
        this.servicio.Servidores.push(val);
      });
    }
  }


}
