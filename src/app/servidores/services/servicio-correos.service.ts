import {Injectable} from '@angular/core';
import {ServicioModel} from '../Models/ServicioModel';
import {CuentaTestModel} from '../Models/CuentaModel';
import {urlServicoCorreos} from '../../Modelos/URLs';



@Injectable()
export class ServicioCorreosService {
  private hosturl = `${urlServicoCorreos}/servicios`;

  private headers = new Headers({
    'Authorization': `Bearer ${localStorage.getItem('token')}`
  });

  constructor() {
  }

  // solicitud de la lista de servidores
  getServicios(): Promise<any> {

    return fetch(this.hosturl, {method: 'GET', headers: this.headers});
  }

  // solicitud de la lista de servidores
  getServicio(id: string): Promise<any> {

    const url = `${this.hosturl}/${id}`;
    return fetch(url, {method: 'GET', headers: this.headers});
  }

  // agregar un objeto Servidor a la db
  add(servidord: ServicioModel): Promise<any> {

    return fetch(this.hosturl, {method: 'POST', body: JSON.stringify(servidord), headers: this.headers});
  }

  // actualizar un objeto Servidor a la db
  update(servidord: ServicioModel): Promise<any> {

    return fetch(this.hosturl, {method: 'PUT', body: JSON.stringify(servidord), headers: this.headers});
  }

  // elimina un objeto Servidor a la db
  delete(servidord: ServicioModel): Promise<any> {

    const url = `${this.hosturl}/d`;
    return fetch(url, {method: 'POST', body: JSON.stringify(servidord), headers: this.headers});
  }

  testEmail(cuenta: CuentaTestModel): Promise<any> {

    const url = `${this.hosturl}/testmail`;
    return fetch(url, {method: 'POST', body: JSON.stringify(cuenta), headers: this.headers});
  }

}
