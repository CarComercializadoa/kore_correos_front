import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import {ServidorModel} from '../Models/ServidorModel';
import {urlServicoCorreos} from '../../Modelos/URLs';



@Injectable()
export class ServidorService {
  private hosturl = `${urlServicoCorreos}/servidores`;

  private headers = new Headers({
    'Authorization': `Bearer ${localStorage.getItem('token')}`
  });


  // private hosturl = 'https://jsonplaceholder.typicodeng serve.com/posts';
  constructor() {
  }


  // solicitud de la lista de servidores
  getServidores(): Promise<any> {

    return fetch(this.hosturl, {method: 'GET', headers: this.headers});
  }

  // agregar un objeto Servidor a la db
  add(servidord: ServidorModel): Promise<any> {

    return fetch(this.hosturl, {method: 'POST', body: JSON.stringify(servidord), headers: this.headers});
  }

  // actualizar un objeto Servidor a la db
  update(servidord: ServidorModel): Promise<any> {

    return fetch(this.hosturl, {method: 'PUT', body: JSON.stringify(servidord), headers: this.headers});
  }

  // elimina un objeto Servidor a la db
  delete(servidord: ServidorModel): Promise<any> {

    const url = `${this.hosturl}/d`;
    return fetch(url, {method: 'POST', body: JSON.stringify(servidord), headers: this.headers});
  }

}
