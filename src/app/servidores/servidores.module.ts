import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ServidoresRoutes} from './servidores.routes';
import {AuthModule} from '../auth/auth.module';
import {MaterialModule} from '../shared/material.module';
import {HttpClientModule} from '@angular/common/http';
import {SharedModule} from '../shared/shared.module';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {MainComponent} from './main.component';
import {ServidorService} from './services/servidor.service';
import {FormsModule} from '@angular/forms';
import {ServidoresComponent} from './servidores/servidores.component';
import {ServiciosComponent} from './servicios/servicios.component';
import {ServicioCorreosService} from './services/servicio-correos.service';
import { ConfiguracionCuentasComponent } from './configuracion-cuentas/configuracion-cuentas.component';
import { ConfiguracionCuentasAltaComponent } from './configuracion-cuentas-alta/configuracion-cuentas-alta.component';
import { ConfiguracionCuentasEditaComponent } from './configuracion-cuentas-edita/configuracion-cuentas-edita.component';
import { AltaComponent } from './servidores/alta/alta.component';
import { EditaComponent } from './servidores/edita/edita.component';
import { ServicioAltaComponent } from './servicios/servicio-alta/servicio-alta.component';
import { ServicioEditaComponent } from './servicios/servicio-edita/servicio-edita.component';
import { IndexComponent } from './index/index.component';

@NgModule({
  imports: [
    CommonModule,
    ServidoresRoutes,
    SharedModule,
    MaterialModule,
    NgxDatatableModule,
    HttpClientModule,
    AuthModule,
    FormsModule
  ],
  declarations: [MainComponent, ServidoresComponent, ServiciosComponent, ConfiguracionCuentasComponent, ConfiguracionCuentasAltaComponent, ConfiguracionCuentasEditaComponent, AltaComponent, EditaComponent, ServicioAltaComponent, ServicioEditaComponent, IndexComponent],
  providers: [ServidorService, ServicioCorreosService]
})
export class ServidoresModule { }
