import {Component, Input, OnInit} from '@angular/core';
import {Notification} from '../../../functions/notification';
import {ServidorModel} from '../../Models/ServidorModel';
import {ServidorService} from '../../services/servidor.service';
import {ServidoresComponent} from '../servidores.component';

@Component({
  selector: 'app-servidores-edita',
  templateUrl: './edita.component.html',
  styleUrls: []
})
export class EditaComponent implements OnInit {

  private Notificacion = new Notification();

  @Input('servidor') servidor: ServidorModel;

    constructor(private servidorService: ServidorService,
                private componentePrincipal: ServidoresComponent) { }

  ngOnInit() {
  }

  actualizaServidor(): void {
    this.servidorService.update(this.servidor)
      .then(response => {
        return response.json();
      }).then( data => {
      if ( data.Codigo === 200 ) {
        console.log( 'data .UPDATE' );
        console.log( data );

        this.Notificacion.showNotification(data.Mensaje, 'success');
        this.servidor = new ServidorModel();
        this.componentePrincipal.getServidores();
        this.componentePrincipal.cancelarActualizacion();
      } else {
        this.Notificacion.showNotification(data.Mensaje, 'danger');
      }
    });
  }

}
