import { Component, OnInit } from '@angular/core';
import {ServidorModel} from '../../Models/ServidorModel';
import {ServidorService} from '../../services/servidor.service';
import {Notification} from '../../../functions/notification';
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ServidoresComponent} from '../servidores.component';

@Component({
  selector: 'app-servidores-alta',
  templateUrl: './alta.component.html',
  styleUrls: []
})
export class AltaComponent implements OnInit {
  // Servidores
  private Notificacion = new Notification();
  public servidor = new ServidorModel();

  constructor(private servidorService: ServidorService,
              private componentePrincipal: ServidoresComponent) { }

  altaServidor(): void {
    this.servidorService.add(this.servidor)
      .then(response => {
        console.log('add');
        console.log(response);
        return response.json();
      }).then(data => {
      if ( data.Codigo === 200 ) {
        console.log( 'data POST' );
        console.log( data );
        this.componentePrincipal.getServidores();
        this.servidor = new ServidorModel();
        this.Notificacion.showNotification(data.Mensaje, 'success');
      } else {
        this.Notificacion.showNotification(data.Mensaje, 'danger');
      }
    });
  }



  ngOnInit() {
  }
}
