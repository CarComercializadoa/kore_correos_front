import { Component, OnInit } from '@angular/core';
import {ServidorService} from '../services/servidor.service';
import {ServidorModel} from '../Models/ServidorModel';
import {Notification} from '../../functions/notification';
import {CuentaModel} from '../Models/CuentaModel';
import {MiServidorModel} from '../Models/MiServidorModel';
import { showLoader } from '../../functions/Utilities';
import swal from "sweetalert2";

@Component({
  selector: 'app-servidores',
  templateUrl: './servidores.component.html',
  styleUrls: []
})
export class ServidoresComponent implements OnInit {

  private Notificacion = new Notification();
  public servidores:  ServidorModel[];
  public servidorToUpd = new ServidorModel();
  public isUpdate = false;

  constructor(private servidorService: ServidorService) { }

  ngOnInit() {
    this.getServidores();
  }

  getServidores(): void {
    this.servidorService.getServidores()
      .then( response => {
        return response.json();
      }).then( data => {
      if ( data.Codigo === 200 ) {
        if ( !data.Data ){
          this.Notificacion.showNotification("Todavia no hay Servidores dados de alta", 'info');
        }
        this.servidores = data.Data;
      } else {
        this.Notificacion.showNotification(data.Mensaje, 'danger');
      }
    });
  }

  cancelarActualizacion(): void {
    this.isUpdate = false;
  }

  editar(servidor: ServidorModel): void {
    this.isUpdate = true;
    this.servidorToUpd.cloneObject(servidor.ID, servidor.Servidor, servidor.Puerto, servidor.MaxDest, servidor.MaxCorreos);
  }



  // Para eliminar
  eliminar(servidor: ServidorModel): void {
    swal({
      title: '¿Eliminar Servidor?',
      text: 'No podras recuperar el registro!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      confirmButtonText: 'Si, Eliminar!',
      buttonsStyling: false
    }).then(function() {
      // Eliminar de db
      this.deleter(servidor);
    }.bind(this)).catch(swal.noop);
  }

  deleter(servidor: ServidorModel): void {
    showLoader(true);
    this.servidorService.delete(servidor)
      .then( response => {
        return response.json();
      })
      .then( data => {
        showLoader(false);
        if ( data.Codigo === 200 ) {
          // Filter
          const servidoresfil = this.servidores.filter( cta => {
            return cta.ID !== servidor.ID;
          });
          this.servidores = servidoresfil;
          swal({
            title: 'Eliminado!',
            text: 'Servidor eliminado.',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
            buttonsStyling: false
          });
        } else {
          //console.log(data);
          this.Notificacion.showNotification(data.Mensaje, 'danger');
          this.getServidores();
        }
      });
  }

}
