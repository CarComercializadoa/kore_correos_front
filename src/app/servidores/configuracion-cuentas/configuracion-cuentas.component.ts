import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ServicioCorreosService} from '../services/servicio-correos.service';
import {ServicioModel} from '../Models/ServicioModel';
import {Notification} from '../../functions/notification';
import {CuentaModel, CuentaTestModel} from '../Models/CuentaModel';
import {ServidorModel} from '../Models/ServidorModel';
import {MiServidorModel} from '../Models/MiServidorModel';
import swal from "sweetalert2";

@Component({
  selector: 'app-configuracion-cuentas',
  templateUrl: './configuracion-cuentas.component.html',
  styleUrls: []
})
export class ConfiguracionCuentasComponent implements OnInit {
  private Notificacion = new Notification();
  public servicio: ServicioModel = new ServicioModel();
  // Update
  public isUpdate = false;
  public cuentaToUpddate = null;

  constructor(
    private route: ActivatedRoute,
    private serviciosService: ServicioCorreosService
  ) { }

  ngOnInit() {
    this.getServicioId();
  }

  getServicioId(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.serviciosService.getServicio(id)
      .then( response => {
        return response.json();
      })
      .then( data => {
        if ( data.Codigo === 200 ) {
          this.servicio = data.Data;
        } else {
          this.Notificacion.showNotification(data.Mensaje, 'danger');
          this.getServicioId();
        }
      });
  }

  editar(cuenta: CuentaModel): void {
    this.isUpdate = true;
    this.cuentaToUpddate = cuenta;
  }
  eliminar(servidor: MiServidorModel, cuenta: CuentaModel): void {
    swal({
    title: '¿Eliminar cuenta?',
    text: 'No podras recuperar el registro!',
    type: 'warning',
    showCancelButton: true,
    confirmButtonClass: 'btn btn-success',
    cancelButtonClass: 'btn btn-danger',
    confirmButtonText: 'Yes, delete it!',
    buttonsStyling: false
  }).then(function() {
    // Filter
    const cuentasfil = servidor.Cuentas.filter( cta => {
      return cta.Correo !== cuenta.Correo;
    });
    servidor.Cuentas = cuentasfil;
    // Eliminar de db
    this.deleter();
  }.bind(this)).catch(swal.noop);
  }

  deleter(): void {
    this.serviciosService.update(this.servicio)
      .then( response => {
        return response.json();
      })
      .then( data => {
        if ( data.Codigo === 200 ) {
          swal({
            title: 'Deleted!',
            text: 'Cuenta eliminada.',
            type: 'success',
            confirmButtonClass: 'btn btn-success',
            buttonsStyling: false
          });
        } else {
          this.getServicioId();
        }
      });
  }

  cancelarActualizacion(): void {
    this.isUpdate = false;
  }

}
