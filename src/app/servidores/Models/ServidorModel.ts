export class ServidorModel {
  public ID: string;
  public Servidor: string;
  public Puerto: string;
  public MaxDest: number;
  public MaxCorreos: number;

  cloneObject(ID: string, Servidor: string, Puerto: string, MaxDest: number, MaxCorreos: number) {
    this.ID = ID;
    this.Servidor = Servidor;
    this.Puerto = Puerto;
    this.MaxDest = MaxDest;
    this.MaxCorreos = MaxCorreos;
  }

}
