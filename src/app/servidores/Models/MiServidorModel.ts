import {CuentaModel} from './CuentaModel';

export class MiServidorModel {
  public ID: string;
  public Orden: number;
  public Servidor: string;
  public Puerto: string;
  public MaxDest: number;
  public MaxCorreos: number;
  public Cuentas: CuentaModel[];

  cloneObject(ID: string, Orden: number, Puerto: string, Servidor: string, MaxDest: number, MaxCorreos: number, Cuentas: CuentaModel[]) {
    this.ID = ID;
    this.Orden = Orden;
    this.Servidor = Servidor;
    this.Puerto = Puerto;
    this.Cuentas = Cuentas;
    this.MaxDest = MaxDest;
    this.MaxCorreos = MaxCorreos;
  }

}
