import {MiServidorModel} from './MiServidorModel';

export class ServicioModel {
  public ID: string;
  public Token: string;
  public Nombre = '';
  public Servidores: MiServidorModel[] = [];
  public Estatus: number;
  public FechaHora: string;



  cloneObject(ID: string, Token: string, Nombre: string, Servidores: MiServidorModel[], Estatus: number, FechaHora: string) {
    this.ID = ID;
    this.Token = Token;
    this.Nombre = Nombre;
    this.Servidores = Servidores;
    this.Estatus = Estatus;
    this.FechaHora = FechaHora;
  }

}
