import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {

  links: any[] = [{
    url: 'servidores',
    img: 'assets/img/servers.jpg',
    nombre: 'Servidores',
    descripcion: 'Alta De Servidores'
  },
    {
      url: 'servicios',
      img: 'assets/img/sending.jpg',
      nombre: 'Servicios',
      descripcion: 'Alta De Servicios'
    }];

  constructor() {
  }

  ngOnInit() {
  }

}
