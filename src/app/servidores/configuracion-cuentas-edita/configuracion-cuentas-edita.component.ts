import {Component, Input, OnInit} from '@angular/core';
import {ServicioModel} from '../Models/ServicioModel';
import {MiServidorModel} from '../Models/MiServidorModel';
import {CuentaModel, CuentaTestModel} from '../Models/CuentaModel';
import {ServicioCorreosService} from '../services/servicio-correos.service';
import {Notification} from '../../functions/notification';
import {ConfiguracionCuentasComponent} from '../configuracion-cuentas/configuracion-cuentas.component';
import {showLoader} from '../../functions/Utilities';

@Component({
  selector: 'app-configuracion-cuentas-edita',
  templateUrl: './configuracion-cuentas-edita.component.html',
  styleUrls: []
})
export class ConfiguracionCuentasEditaComponent implements OnInit {

  private Notificacion = new Notification();

  @Input('servidor') servidor: MiServidorModel;
  @Input('servicio') servicio: ServicioModel;
  @Input('cuenta') cuenta: CuentaModel;

  public cuentaTemporal = new CuentaModel();
  public isValid = false;
  public WarningMessage = null; // Par amostrar advertencia de configuracion de cuenta
  public confirmNum = null;
  public codigo = '';

  constructor(
    private serviciosService: ServicioCorreosService,
    private componentePrincipal: ConfiguracionCuentasComponent) { }

  ngOnInit() {
    this.cuentaTemporal.Correo = this.cuenta.Correo;
    this.cuentaTemporal.Password = this.cuenta.Password;
  }



  // ###### FUNCIONES PARA UN ALTA ##### //

  actualizarServicio(): void {
    showLoader(true);
    this.cuenta.Correo = this.cuentaTemporal.Correo;
    this.cuenta.Password = this.cuentaTemporal.Password;
    this.serviciosService.update(this.servicio)
      .then( response => {
        return response.json();
      })
      .then( data => {
        showLoader(false);
        if ( data.Codigo === 200 ) {
          this.Notificacion.showNotification(data.Mensaje, 'success');
          this.componentePrincipal.cancelarActualizacion();
        } else {
          this.Notificacion.showNotification(data.Mensaje, 'danger');
        }
      });
  }


  testEmail(): void {
    showLoader(true);
    const cuentaPrueba = new CuentaTestModel();
    cuentaPrueba.Correo = this.cuentaTemporal.Correo;
    cuentaPrueba.Password = this.cuentaTemporal.Password;
    cuentaPrueba.Servidor = this.servidor.Servidor;
    cuentaPrueba.Puerto = this.servidor.Puerto;

    if (cuentaPrueba.Correo === "" || cuentaPrueba.Password === ""){
      this.Notificacion.showNotification('Es necesario especificar sus credenciales de correo para poder continuar.', 'danger');
      showLoader(false);
    }else{
      if (!this.validacorreo(cuentaPrueba.Correo)){
        this.Notificacion.showNotification('Formato de correo electrónico inválido.', 'danger');
        showLoader(false);
      }else{
        this.serviciosService.testEmail(cuentaPrueba)
          .then( response => {
            return response.json();
          })
          .then( data => {
            showLoader(false);
            if ( data.Codigo === 200 ) {
              this.confirmNum = data.Data;
              this.Notificacion.showNotification(data.Mensaje, 'success');
            } else {
              this.WarningMessage = null;
              if ( data.Data ) {
                const  err = data.Data[0].split(' ');
                if (err[0] === '534' ) {
                  this.WarningMessage = 'Debes configurar el inicio de sesion con otras aplicaciones desde tu cuenta de correo.';
                }
                if (err[0] === '535' ) {
                  this.WarningMessage = 'Usuario y contraseña incorrectos';
                }
                
              }
              this.Notificacion.showNotification(data.Mensaje, 'danger');
            }
          });
        }
    }
  }

  confirmarCodigo(): void {
    if ( this.codigo === this.confirmNum ) {
      this.isValid = true;
    }else {
      this.isValid = false;
    }
  }
  resetDatos(): void {
    this.isValid = false;
    this.confirmNum = null;
  }

  validacorreo(correo: string): boolean{
    var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    return re.test(correo)
  }



}
