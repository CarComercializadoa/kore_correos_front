import {Component, Input, OnInit} from '@angular/core';
import {ServicioModel} from '../Models/ServicioModel';
import {ServicioCorreosService} from '../services/servicio-correos.service';
import {Notification} from '../../functions/notification';
import {MiServidorModel} from '../Models/MiServidorModel';
import {CuentaModel, CuentaTestModel} from '../Models/CuentaModel';
import {ActivatedRoute} from '@angular/router';
import {split} from 'ts-node/dist';
import {showLoader} from '../../functions/Utilities';
import {ConfiguracionCuentasComponent} from '../configuracion-cuentas/configuracion-cuentas.component';

@Component({
  selector: 'app-configuracion-cuentas-alta',
  templateUrl: './configuracion-cuentas-alta.component.html',
  styleUrls: []
})
export class ConfiguracionCuentasAltaComponent implements OnInit {
  private Notificacion = new Notification();

  @Input('servidor') servidor: MiServidorModel;
  @Input('servicio') servicio: ServicioModel;

  // Vars Alta
  public cuenta = new CuentaModel();
  public cuentaPrueba = new CuentaTestModel();
  public isValid= false;
  public WarningMessage = null; // Par amostrar advertencia de configuracion de cuenta
  public codigo = '';
  public confirmNum = null;


  constructor(
    private route: ActivatedRoute,
    private serviciosService: ServicioCorreosService,
    private mainComopnent: ConfiguracionCuentasComponent
  ) { }

  ngOnInit() {
    this.resetDatos();
  }



  // ###### FUNCIONES PARA UN ALTA ##### //

  actualizarServicio(): void {
    showLoader(true);
    this.serviciosService.update(this.servicio)
      .then( response => {
        return response.json();
      })
      .then( data => {
        showLoader(false);
        if ( data.Codigo === 200 ) {
          this.Notificacion.showNotification(data.Mensaje, 'success');
          this.cuentaPrueba = new CuentaTestModel();
          this.resetDatos();
        } else {
          this.Notificacion.showNotification(data.Mensaje, 'danger');
          if ( data.Data ) {
            data.Data.forEach( dat => {
              this.Notificacion.showNotification(dat, 'danger');
            });
          }
          this.mainComopnent.getServicioId();
        }
      });
  }

  guardarCuenta(): void {
    const nuevaCuenta = new CuentaModel();
    nuevaCuenta.Password = this.cuentaPrueba.Password;
    nuevaCuenta.Correo = this.cuentaPrueba.Correo;
    if ( this.servidor.Cuentas ) {
      this.servidor.Cuentas.push(nuevaCuenta);
    }else {
      const ar = [];
      ar.push(nuevaCuenta);
      this.servidor.Cuentas = ar;
    }
    this.actualizarServicio();
  }

  testEmail(server: string, puerto: string): void {
    showLoader(true);
    this.cuentaPrueba.Servidor = server;
    this.cuentaPrueba.Puerto = puerto;

    if (this.cuentaPrueba.Correo === undefined || this.cuentaPrueba.Password === undefined){
      this.Notificacion.showNotification('Es necesario especificar sus credenciales de correo para poder continuar.', 'danger');
      showLoader(false);
    }else{
      if (!this.validacorreo(this.cuentaPrueba.Correo)){
        this.Notificacion.showNotification('Formato de correo electrónico inválido.', 'danger');
        showLoader(false);
      }else{
        this.serviciosService.testEmail(this.cuentaPrueba)
          .then( response => {
            return response.json();
          })
          .then( data => {
            showLoader(false);
            if ( data.Codigo === 200 ) {
              this.confirmNum = data.Data;
              this.Notificacion.showNotification(data.Mensaje, 'success');
            } else {
              this.WarningMessage = null;
              if ( data.Data ) {
                const  err = data.Data[0].split(' ');
                if (err[0] === '534' ) {
                  this.WarningMessage = 'Debes configurar el inicio de sesion con otras aplicaciones desde tu cuenta de correo.';
                }
                if (err[0] === '535' ) {
                  this.WarningMessage = 'Usuario y contraseña incorrectos';
                }
              }
              this.Notificacion.showNotification(data.Mensaje, 'danger');
            }
          });
        }
    }
  }

  confirmarCodigo(): void {
    if ( this.codigo === this.confirmNum ) {
      this.isValid = true;
    }else {
      this.isValid = false;
    }
  }
  resetDatos(): void {
    this.isValid = false;
    this.confirmNum = null;
  }

  validacorreo(correo: string): boolean{
    var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    return re.test(correo)
  }

}
