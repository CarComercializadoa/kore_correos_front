import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';


import {AppComponent} from './app.component';
import {CajasModule} from './cajas/cajas.module';
import {LoginComponent} from './login/login.component';
import {AppRoutes} from './app.routing';
import {FormsModule} from '@angular/forms';

import {HttpClientModule} from '@angular/common/http';
import {FieldErrorDisplayComponent} from './components/field-error-display/field-error-display.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ServidoresModule} from './servidores/servidores.module';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    FieldErrorDisplayComponent,
  ],
  imports: [
    BrowserModule,
    CajasModule,
    ServidoresModule,
    AppRoutes,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
