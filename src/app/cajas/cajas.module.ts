import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MainComponent} from './main.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {CajasRoutes} from './cajas.routes';
import {SharedModule} from '../shared/shared.module';
import {TableComponent} from '../components/table/table.component';
import {TableActionsComponent} from '../components/table-actions/table-actions.component';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {MaterialModule} from '../shared/material.module';



import {HttpClientModule} from '@angular/common/http';
import {HttpModule} from '@angular/http';
import {AuthGuardService} from '../auth/auth-guard.service';
import {AuthService} from '../auth/auth.service';
import {AuthModule} from '../auth/auth.module';


@NgModule({
  imports: [
    CommonModule,
    CajasRoutes,
    SharedModule,
    MaterialModule,
    NgxDatatableModule,
    HttpClientModule,
    AuthModule
  ],
  declarations: [MainComponent, DashboardComponent],
  providers: [
    ]
})
export class CajasModule {
}
