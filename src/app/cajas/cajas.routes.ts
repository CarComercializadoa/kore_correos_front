import {RouterModule, Routes} from '@angular/router';
import {MainComponent} from './main.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {AuthGuardService} from '../auth/auth-guard.service';

const cajasRoutes: Routes = [
  {
    path: 'cajas',
    component: MainComponent,
    children: [
      {path: 'dashboard', component: DashboardComponent},
      {path: '', redirectTo: '/cajas/dashboard', pathMatch: 'full'},
    ],
    canActivate: [AuthGuardService]
  }
];

export const CajasRoutes = RouterModule.forChild(cajasRoutes);
