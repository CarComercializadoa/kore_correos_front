import {Component, OnInit} from '@angular/core';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styles: []
})

export class DashboardComponent implements OnInit {
  tabla2: any;
  rows = [
    {name: 'Austin', gender: 'Male', company: 'Swimlane'},
    {name: 'Dany', gender: 'Male', company: 'KFC'},
    {name: 'Molly', gender: 'Female', company: 'Burger King'},
  ];
  columns = [
    {prop: 'name'},
    {name: 'Gender'},
    {name: 'Company'}
  ];

  constructor() {
    setTimeout(() => {
      this.tabla2.rows.push({
        data: [{value: '3'}, {value: 'item 2'}, {value: 'Es un texto diferente'}],
        actions: {delete: true, edit: true}
      });
    }, 2000);
  }

  ngOnInit() {
    this.tabla2 = {
      headers: ['id', 'Nombre', 'Texto', 'Acciones'],
      rows: [{
        data: [{value: '0'}, {value: 'item 1'}, {value: 'Este es un texto grande'}],
        actions: {edit: true, details: true, delete: true}
      },
        {
          data: [{value: '1'}, {value: 'item 2'}, {value: 'Este es un texto grande'}],
          actions: {edit: true, details: true, delete: true}
        }
      ]
    };


  }

  selectRow(r) {
    console.log(r);
  }

  dataTableSelectRow(e) {
    console.log('Seleccionado: ', e);
  }
}
