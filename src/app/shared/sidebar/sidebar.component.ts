import {Component, OnInit} from '@angular/core';
import PerfectScrollbar from 'perfect-scrollbar';
import {SidebarService} from '../../services/sidebar.service';
import {AuthService} from '../../auth/auth.service';
import {PerfilUsuario} from '../../Modelos/Respuesta.modelo';

declare var $: any;

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styles: []
})
export class SidebarComponent implements OnInit {

  public menuItems: any[];

  public profile: PerfilUsuario;

  constructor(public _sidebar: SidebarService, public auth: AuthService) {
    /*this._sidebar.getMenus().subscribe(d => {
      this.routes = d;
    });*/

    this.profile = this.auth.getProfile();
  }

  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  }

  getMenuItems(): any {
    return this.menuItems;
  }

  ngOnInit() {
    this.menuItems = this._sidebar.ROUTES.filter(menuItem => menuItem);
    this.updatePS();
  }

  updatePS(): void {
    if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
      const elemSidebar = <HTMLElement>document.querySelector('.sidebar .sidebar-wrapper');
      const ps = new PerfectScrollbar(elemSidebar, {wheelSpeed: 2, suppressScrollX: true});
    }
  }

  isMac(): boolean {
    let bool = false;
    if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
      bool = true;
    }
    return bool;
  }
}
