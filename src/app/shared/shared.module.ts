import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NavbarComponent} from './navbar/navbar.component';
import {SidebarComponent} from './sidebar/sidebar.component';
import {NotFoundComponent} from './not-found/not-found.component';
import {SidebarService} from '../services/sidebar.service';
import {NavbarService} from '../services/navbar.service';
import {RouterModule} from '@angular/router';
import {AuthModule} from '../auth/auth.module';
import {HttpModule} from '@angular/http';
import {TableComponent} from '../components/table/table.component';
import {TableActionsComponent} from '../components/table-actions/table-actions.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    AuthModule,
    HttpModule
  ],
  declarations: [
    NavbarComponent,
    SidebarComponent,
    NotFoundComponent
    , TableComponent, TableActionsComponent
  ],
  providers: [SidebarService, NavbarService],
  exports: [NavbarComponent, SidebarComponent, NotFoundComponent, TableComponent, TableActionsComponent]
})
export class SharedModule {
}
