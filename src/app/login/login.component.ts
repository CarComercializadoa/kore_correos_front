import {Component, OnInit} from '@angular/core';
import {AuthService} from '../auth/auth.service';
import {Notification} from '../functions/notification';
import {showLoader} from '../functions/Utilities';

declare var $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit {
  usuario: string = '';
  password: string = '';
  sending = false;
  private Notificacion = new Notification();

  constructor(private _auth: AuthService) {
  }

  ngOnInit() {
    setTimeout(function () {
      $('.card').removeClass('card-hidden');
    }, 700);

    
  }

  login(ev) {
    if (this.usuario.length > 0 && this.password.length > 0) {
      showLoader(true);
      this.sending = true;
      this._auth.login(this.usuario, this.password)
        .subscribe((d: boolean) => {
            showLoader(false);
            this.sending = false;
            if (d) {
              this.Notificacion.duration = 500;
              this.Notificacion.showNotification('Sesion iniciada', 'success');
            }
          },
          e => {
            console.log( e );
            this.sending = false;
            showLoader(false);
            this.Notificacion.showNotification(e.error.Mensaje ? e.error.Mensaje : 'No se pudo iniciar sesión',
              'danger');
          });
    } else {
      this.Notificacion.showNotification('Ingrese nombre y contraseña', 'danger');
    }
  }
}
