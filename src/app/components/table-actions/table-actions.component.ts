import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {RowWithActionsModelo, TableWithActionsModelo} from '../../Modelos/RowWithActions.modelo';

@Component({
  selector: 'app-table-extend',
  templateUrl: './table-actions.component.html',
  styles: []
})
export class TableActionsComponent implements OnInit {
  @Input() title = 'Tabla con acciones';
  @Input('Datos') data: TableWithActionsModelo;
  @Output() onSelectRow: EventEmitter<RowWithActionsModelo> = new EventEmitter<RowWithActionsModelo>();

  constructor() {
  }

  ngOnInit() {
  }

  deleteRow(index: number, row: RowWithActionsModelo) {
    this.data.rows.splice(index, 1);
  }

  selectRow(index: number, row: RowWithActionsModelo) {
    this.onSelectRow.emit(row);
  }
}
