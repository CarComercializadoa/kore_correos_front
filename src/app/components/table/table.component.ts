import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styles: []
})
export class TableComponent implements OnInit {
  @Input() title = 'Tabla simple';
  @Input() headers: string[] = [];
  @Input() rows: any[] = [];

  constructor() {
  }

  ngOnInit() {
  }

}
