import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {AuthService} from './auth.service';

@Injectable()
export class AuthAdminService implements CanActivate {

  constructor(private auth: AuthService, private route: Router) {
  }

  canActivate(): boolean {
    if (this.auth.isAuth() && this.auth.isAdmin()) {
      return true;
    }
    this.route.navigate(['/cajas/']);
    return false;
  }
}
