import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/map';
import {RespuestaModelo, PerfilUsuario} from '../Modelos/Respuesta.modelo';
import {UrlLogin} from '../Modelos/URLs';
import {Router} from '@angular/router';
import {JwtHelper, tokenNotExpired} from 'angular2-jwt';


@Injectable()
export class AuthService {
  jwtHelper: JwtHelper = new JwtHelper();

  constructor(private _http: HttpClient, private _router: Router) {
  }

  login(username: string, password: string) {
    return this._http.post(UrlLogin, JSON.stringify({User: username, Pass: password}))
      .map((d: RespuestaModelo) => {
        localStorage.setItem('token', d.Data);
        this._router.navigate([`/servicioCorreos`]);
        return true;
      });
  }

  isAuth(): boolean {
    return tokenNotExpired();
  }

  logout() {
    localStorage.removeItem('token');
    this._router.navigate(['login']);
  }

  isAdmin(): boolean {
    const token = localStorage.getItem('token');
    if (token) {
      const decoded = this.jwtHelper.decodeToken(token);
      return decoded.isAdmin;
    }
    return false;
  }

  getProfile(): PerfilUsuario {
    const token = localStorage.getItem('token');
    if (token) {
      const decoded = this.jwtHelper.decodeToken(token);
      return {User: decoded.username};
    }
    return null;
  }
}
