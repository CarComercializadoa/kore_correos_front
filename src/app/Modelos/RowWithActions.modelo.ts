export interface TableWithActionsModelo {
  headers: string[];
  rows: RowWithActionsModelo[];
}

export interface RowWithActionsModelo {
  data: DataCell[];
  actions: ActionsRow;
}

export interface ActionsRow {
  details?: boolean;
  edit?: boolean;
  delete?: boolean;
}

export interface DataCell {
  type?: string;
  value: any;
}
