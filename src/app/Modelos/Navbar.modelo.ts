export interface NotificationModel {
  type: string;
  text: string;
  id: string;
}
