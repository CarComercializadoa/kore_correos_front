import {environment} from '../../environments/environment';

// urlLogin: window.location.origin,
export const UrlLogin = environment.production ? window.location.origin + '/login' : 'http://10.0.1.235:8080/login';


// Servicio de Correos
export const urlServicoCorreos = environment.production ? window.location.origin : 'http://10.0.1.235:8080';
