export interface RespuestaModelo {
  Mensaje: string;
  Data: any;
  Codigo: number;
}

export interface PerfilUsuario {
  User: string;
}
