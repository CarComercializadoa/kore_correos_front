export function showLoader(mode = true) {
  if (mode) {
    document.getElementById('loader').classList.remove('hide');
  } else {
    document.getElementById('loader').classList.add('hide');
  }
}
