declare var $: any;

export class Notification {
  private _message = 'notificacion';
  private _from = 'top';
  private _align = 'right';
  private _type = 'success';
  private _duration: number;

  constructor(message = 'notificacion', type = 'success', duration = 3000, from?: string, align?: string) {
    this._message = message;
    this._type = type;
    this._duration = duration;
    this._align = align ? align : 'right';
    this._from = from ? from : 'top';
  }

  showNotification(message: string, type: string) {
    this.message = message;
    this.type = type;
    $.notify({
        icon: 'notifications',
        message: this._message
      },
      {
        type: this._type,
        timer: this._duration,
        placement:
          {
            from: this._from,
            align: this._align
          }
      }
    );

  }


  set message(value: string) {
    this._message = value;
  }

  set from(value: string) {
    this._from = value;
  }

  set align(value: string) {
    this._align = value;
  }

  set type(value: string) {
    this._type = value;
  }

  set duration(value: number) {
    this._duration = value;
  }
}
