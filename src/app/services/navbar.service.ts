import {Injectable} from '@angular/core';
import {NotificationModel} from '../Modelos/Navbar.modelo';
import {AuthHttp} from 'angular2-jwt';

@Injectable()
export class NavbarService {
  public notifications: NotificationModel[] = [{id: '123', text: 'Se ha solicitado apertura  de una caja', type: 'AC'}];

  constructor(public http: AuthHttp) {
  }


}
