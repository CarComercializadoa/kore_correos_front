import {Injectable} from '@angular/core';
import {RouteInfo} from '../Modelos/SidebarRoute.modelo';
import {AuthHttp} from 'angular2-jwt';

@Injectable()
export class SidebarService {
  public ROUTES: RouteInfo[] = [{
    path: '/servicioCorreos/servidores',
    title: 'Servidores',
    type: 'link',
    icontype: 'mail_outline'
  },{
    path: '/servicioCorreos/servicios',
    title: 'Servicios',
    type: 'link',
    icontype: 'contact_mail'
  },/* {
    path: '/servicioCorreos',
    title: 'Servicio de Correos',
    type: 'sub',
    icontype: 'image',
    collapse: 'pages',
    children: [
      {path: 'servidores', title: 'Servidores', ab: 'S'},
      {path: 'servicios', title: 'Servicios', ab: 'SC'}
    ]
  }*/
  ];

  constructor() {
  }
}
